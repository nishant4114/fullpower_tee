class AddSizeToVariants < ActiveRecord::Migration
  def change
    add_column :variants, :size, :string
  end
end
