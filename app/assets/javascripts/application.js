//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.placeholder.min
//= require jquery.tooltips
//= require chosen.jquery
//= require sequence
//= require slick
//= require custom
//= require jquery.smartmenus
//= require jquery.smartmenus.bootstrap
//= require_tree




//$(function(){ $(document).foundation(); });
$(document).ready(function() {
// THIS CODE WILL HAVE A LINK OPEN IN A NEW POPUP
  $(document).on('click', 'a[data-popup]', function (e) {
    window.open($(this)[0].href);
    e.preventDefault();
  });

//THIS CODE WILL DISABLE ANY AUTOCOMPLETE for CSS class == disableAutoComplete
  if (document.getElementsByTagName) {
    var inputElements = document.getElementsByTagName("input");
    for (i = 0; inputElements[i]; i++) {
      if (inputElements[i].className && (inputElements[i].className.indexOf("disableAutoComplete") != -1)) {
        inputElements[i].setAttribute("autocomplete", "off");
      }//if current input element has the disableAutoComplete class set.
    }//loop thru input elements
  }//basic DOM-happiness-check

// This allows forms to have unobtrusive JS nested forms.

});