class Admin::Merchandise::Multi::VariantsController < Admin::BaseController
  helper_method :image_groups
  def edit
    @product        = Product.includes(:properties,:product_properties, {:prototype => :properties}).find(params[:product_id])
    form_info
    render :layout => 'admin_markup'
  end

  def update
    @product = Product.find(params[:product_id])
p @product
    if @product.update_attributes(allowed_params)
      p 'saved'
      flash[:notice] = "Successfully updated variants"
      redirect_to admin_merchandise_product_url(@product)
    else
      p 'not saved'
      form_info
      render :action => :edit, :layout => 'admin_markup'
    end
  end
  private


    def allowed_params
      params.require(:product).permit(:name,  :id, :description, :product_keywords, :set_keywords, :product_type_id,
                                     :prototype_id, :shipping_category_id, :permalink, :available_at, :deleted_at,
                                     :meta_keywords, :meta_description, :featured, :description_markup, :brand_id,
                                     product_properties_attributes: [:product_id, :property_id, :position,  :description],
                                      :variants_attributes => [:id, :product_id, :sku, :name, :size, :price, :cost, :image_group_id, :inactivate, :deleted_at, :master, :brand_id, :inventory_id, :variant_properties_attributes => [:id, :primary, :property_id, :description]]
                                      )
      #permit({:variants_attributes => [:id, :product_id, :sku, :name, :price, :cost, :deleted_at, :master, :brand_id, :inventory_id]} )
    end

  def image_groups
    @image_groups ||= ImageGroup.where(:product_id => @product).map{|i| [i.name, i.id]}
  end

  def form_info
  end
end

