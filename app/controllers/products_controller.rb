class ProductsController < ApplicationController

  def index
    params[:filterrific][:search_query] = params[:search_query] unless params[:filterrific].nil?
    params[:filterrific][:with_product_type] = params[:with_product_type] unless params[:filterrific].nil?
    params[:filterrific][:with_tag] = params[:with_tag] unless params[:filterrific].nil?
    params[:filterrific][:with_min_range] = params[:with_min_range] unless params[:filterrific].nil?
    params[:filterrific][:with_max_range] = params[:with_max_range] unless params[:filterrific].nil?

    @filterrific = initialize_filterrific(
        Product.joins(:variants),
        params[:filterrific],
        :select_options => {
            # sorted_by: Event.options_for_sorted_by,
            # with_category_id: Category.options_for_select

        },
        persistence_id: false
    ) or return
    if params[:product_type_id]
     @products = Product.where(product_type_id: params[:product_type_id],deleted_at: nil).all
    elsif params[:search_query]
    @products = Product.where("products.name LIKE ?", "%#{params[:search_query]}%")
    # elsif params[:product_keywords] && params[:product_type_id]
    #   @products = Product.where("products.product_keywords like(?) AND product_type_id = ?", ["%#{params[:product_keywords]}%", "%#{params[:product_type_id]}%"]).all
    else
      @products = @filterrific.find.active.includes(:variants)
    end
    form_info

    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    if params[:q] && params[:q].present?
      @products = Product.standard_search(params[:q]).results
    else
      @products = Product.where('deleted_at IS NULL OR deleted_at > ?', Time.zone.now )
    end

    render :template => '/products/index'
  end

  def show
    @product = Product.active.find(params[:id])
    form_info
    @cart_item.variant_id = @product.active_variants.first.try(:id)

    respond_to do |format|
      format.html  #for my controller, i wanted it to be JS only
      format.js
    end
  end


  private

  def form_info
    @cart_item = CartItem.new
  end

  def featured_product_types
    [ProductType::FEATURED_TYPE_ID]
  end
end
