class WelcomeController < ApplicationController



  def index
    @featured_product = Product.where(featured: true)
    @best_selling_products = Product.limit(5)
    @other_products  ## search 2 or 3 categories (maybe based on the user)
    unless @featured_product
      # if current_user && current_user.admin?
      #   redirect_to admin_merchandise_products_url
      # else
      #   redirect_to login_url
      # end
    end
    p @featured_product

    # code for products
    products = Product.active.includes(:variants)

    product_types = nil
    if params[:product_type_id].present? && product_type = ProductType.find_by_id(params[:product_type_id])
      product_types = product_type.self_and_descendants.map(&:id)
    end
    if product_types
      @products = products.where(product_type_id: product_types)
    else
      @products = products
    end
    p 'listing products'
    p @products

  end

  def about_us

  end

  def policy

  end

  def privacy_policy

  end

  def terms

  end
end
